package mandysax.fragment

import mandysax.lifecycle.ViewModel
import mandysax.lifecycle.ViewModelProviders

inline fun <reified VM : ViewModel> Fragment.viewModels() = lazy {
    return@lazy ViewModelProviders.of(
        viewLifecycleOwner
    )[VM::class.java]
}