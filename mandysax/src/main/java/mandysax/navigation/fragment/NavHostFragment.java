package mandysax.navigation.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import mandysax.fragment.Fragment;
import mandysax.navigation.NavController;

/**
 * @author Huang hao
 */
public class NavHostFragment extends Fragment {

    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        NavHostFragmentView mRoot = new NavHostFragmentView(this);
        mRoot.setId(View.generateViewId());
        return mRoot;
    }

    @NonNull
    public NavController getNavController() {
        return new NavController(this);
    }

    @NonNull
    public static NavHostFragment create(Fragment fragment) {
        NavHostFragment navHostFragment = new NavHostFragment();
        navHostFragment.getNavController().navigate(fragment);
        return navHostFragment;
    }

}
