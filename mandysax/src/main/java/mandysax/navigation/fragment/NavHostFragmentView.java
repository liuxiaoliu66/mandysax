package mandysax.navigation.fragment;

import android.annotation.SuppressLint;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import mandysax.navigation.fragment.NavHostFragment;

/**
 * @author Huang hao
 */
@SuppressLint("ViewConstructor")
public final class NavHostFragmentView extends FrameLayout {

    private final NavHostFragment mFragment;

    NavHostFragmentView(@NonNull NavHostFragment navHostFragment) {
        super(navHostFragment.getContext());
        mFragment = navHostFragment;
    }

    @NonNull
    public NavHostFragment getNavHostFragment() {
        return mFragment;
    }

}
