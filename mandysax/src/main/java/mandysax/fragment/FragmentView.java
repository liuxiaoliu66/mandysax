package mandysax.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import mandysax.core.R;
import mandysax.lifecycle.LifecycleOwner;
import mandysax.lifecycle.ViewModelProviders;
import mandysax.lifecycle.ViewTreeLifecycleOwner;

/**
 * @author Huang hao
 */
public final class FragmentView extends FrameLayout {

    private final String mName;
    private final String mTag;
    private Fragment mFragment;

    @SuppressLint("CustomViewStyleable")
    public FragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Fragment);
        mName = typedArray.getString(R.styleable.Fragment_android_name);
        String tag = typedArray.getString(R.styleable.Fragment_android_tag);
        if (tag == null)
            tag = mName + getId();
        mTag = tag;
        typedArray.recycle();
    }

    public FragmentView(Context context, @NonNull Fragment fragment) {
        super(context);
        mName = null;
        mTag = fragment.toString();
        mFragment = fragment;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mFragment = getFragment();

        if (mFragment.isAdded()) {
            if (mFragment.getRoot() != null && mFragment.getRoot().getParent() == null) {
                addView(mFragment.getRoot());
                mFragment.onHiddenChanged(false);
            }
            return;
        }

        Fragment parentFragment = getParentFragment();
        ViewModelProviders.of(requireActivity()).get(FragmentManagerViewModel.class)
                .mController
                .dispatchAdd(parentFragment, mFragment, getId(), getTag());
        mFragment.setTag(getTag());
        mFragment.mLayoutId = 0;
        mFragment.onHiddenChanged(false);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mFragment != null)
            mFragment.onHiddenChanged(true);
        mFragment = null;
    }

    @Nullable
    public String getTag() {
        return mTag;
    }

    public Fragment getFragment() {
        if (mFragment == null) {
            mFragment = requireActivity().getFragmentPlusManager().findFragmentByTag(getTag());
        }
        if (mFragment == null && mName != null) {
            try {
                mFragment = (Fragment) Class.forName(mName).newInstance();
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                throw new RuntimeException("Failed to instantiate fragment");
            }
        }
        return mFragment;
    }

    @Nullable
    private Fragment getParentFragment() {
        ViewGroup parent = (ViewGroup) getParent();
        while (parent != null) {
            if (parent.getId() == android.R.id.content) {
                return null;
            }
            Fragment fragment = requireActivity().getFragmentPlusManager().findFragmentById(parent.getId());
            if (fragment != null)
                return fragment;
            if (parent.getParent() != null) {
                parent = (ViewGroup) parent.getParent();
            }
        }
        return null;
    }

    /**
     * requireActivity
     *
     * @return fragmentActivity
     */
    @NonNull
    private FragmentActivity requireActivity() {
        LifecycleOwner owner = ViewTreeLifecycleOwner.get(this);
        if (owner instanceof FragmentActivity) {
            return (FragmentActivity) owner;
        }
        if (getContext() instanceof FragmentActivity)
            return (FragmentActivity) getContext();
        Activity activity = (Activity) getRootView().findViewById(android.R.id.content).getContext();
        if (activity instanceof FragmentActivity)
            return (FragmentActivity) activity;
        throw new NullPointerException("FragmentActivity not found");
    }

}
