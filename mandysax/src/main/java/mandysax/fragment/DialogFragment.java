package mandysax.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;

/**
 * @author Huang hao
 */
public class DialogFragment extends Fragment implements DialogInterface.OnDismissListener, DialogInterface.OnCancelListener {

    private DialogInterface.OnCancelListener mCancelListener;

    private DialogInterface.OnDismissListener mDismissListener;

    private Dialog mDialog;

    @StyleRes
    public int getTheme() {
        return 0;
    }

    @CallSuper
    @Override
    protected void onDestroyView() {
        super.onDestroyView();
        mView = null;
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.setOnDismissListener(null);
            mDialog.dismiss();
            mDialog = null;
        }
        dismissDialog();
    }

    @CallSuper
    @Override
    protected void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        mDialog = onCreateDialog(bundle);
        mDialog.setOnCancelListener(this);
        mDialog.setOnDismissListener(this);
        View root = onCreateView(getLayoutInflater(), (ViewGroup) mDialog.getWindow().getDecorView());
        if (root != null) {
            mDialog.setContentView(root);
            onViewCreated(root);
        }
        mDialog.show();
    }

    /**
     * 构建待显示的Dialog
     *
     * @param savedInstanceState SavedInstanceState
     * @return 你的Dialog
     */
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireActivity(), getTheme());
    }

    /**
     * 获取你的Dialog
     *
     * @return 你的Dialog
     */
    @Nullable
    public Dialog getDialog() {
        return mDialog;
    }

    /**
     * 显示DialogFragment
     *
     * @param manager FragmentManage
     */
    public void show(@NonNull FragmentManager manager) {
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(0, this);
        ft.commit();
    }

    /**
     * 显示DialogFragment
     *
     * @param transaction Fragment事务
     * @return 事务id
     */
    public int show(@NonNull FragmentTransaction transaction) {
        transaction.add(0, this);
        return transaction.commit();
    }

    private void dismissDialog() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    public void dismiss() {
        dismissDialog();
    }

    public void setCanceledOnTouchOutside(boolean cancel) {
        if (mDialog != null) {
            mDialog.setCanceledOnTouchOutside(cancel);
        }
    }

    public void setCancelable(boolean cancel) {
        if (mDialog != null) {
            mDialog.setCancelable(cancel);
        }
    }

    @Override
    public <T extends View> T findViewById(int i) {
        if (mDialog != null) {
            return mDialog.findViewById(i);
        }
        return null;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        if (mCancelListener != null) {
            mCancelListener.onCancel(dialog);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (mDismissListener != null && dialog != null) {
            mDismissListener.onDismiss(dialog);
        }
        mDialog = null;
        getChildFragmentManager().beginTransaction().remove(this).commitNow();
    }

    public void setOnCancelListener(DialogInterface.OnCancelListener listener) {
        mCancelListener = listener;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        mDismissListener = listener;
    }

    @Override
    public boolean isVisible() {
        return mDialog != null && mDialog.isShowing();
    }

    @Override
    public boolean isHidden() {
        return !isVisible();
    }
}
