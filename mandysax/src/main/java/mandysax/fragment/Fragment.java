package mandysax.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.jetbrains.annotations.Contract;

import mandysax.lifecycle.Lifecycle;
import mandysax.lifecycle.LifecycleOwner;
import mandysax.lifecycle.LifecycleRegistry;

/**
 * @author Huang hao
 */
@SuppressWarnings("unused")
public class Fragment implements LifecycleOwner {
    /**
     * {@link Fragment}{@link View}的{@link LifecycleOwner}
     *
     * @see FragmentViewLifecycleOwner
     */
    final FragmentViewLifecycleOwner mViewLifecycleOwner = new FragmentViewLifecycleOwner();
    /**
     * {@link Fragment}是否已从{@link FragmentActivity}中解绑
     */
    boolean mDetached = true;
    /**
     * {@link Fragment}的父{@link View}的Id
     * <p>为0时表示此{@link Fragment}不存在父{@link View}
     */
    @IdRes
    int mLayoutId = 0;
    /**
     * {@link Fragment}是否添加到{@link FragmentActivity}中
     */
    boolean mAdded = false;
    /**
     * {@link Fragment}是否正常从{@link FragmentActivity}中移除
     */
    boolean mRemoving = false;
    /**
     * {@link Fragment}的Tag
     */
    String mTag;
    /**
     * {@link Fragment}托管的{@link View}
     */
    View mView;
    /**
     * {@link android.app.Activity}的{@link Bundle}
     */
    Bundle mArguments;
    /**
     * @see FragmentActivity
     */
    @Nullable
    FragmentActivity mActivity;
    /**
     * 是否在配置变更时保留{@link Fragment}的{@link View}
     */
    boolean mRetainInstance;

    /**
     * @return FragmentPlusManager for activity
     */
    @NonNull
    public final FragmentManager getFragmentManager() {
        return requireActivity().getFragmentPlusManager();
    }

    /**
     * @return FragmentPlusManager for this fragment
     */
    @NonNull
    public final FragmentManager getChildFragmentManager() {
        return requireActivity().getFragmentController().getFragmentManager(this);
    }

    @NonNull
    public final FragmentManager getParentFragmentManager() {
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            parentFragment.getChildFragmentManager();
        }
        return getFragmentManager();
    }

    @Nullable
    public final Fragment getParentFragment() {
        FragmentStore.Store store = requireActivity().getFragmentController().findStore(mTag);
        if (store != null) {
            if (store.parentStore != null) {
                return store.parentStore.fragment;
            }
        }
        return null;
    }

    @NonNull
    public final Fragment requireParentFragment() {
        Fragment parentFragment = getParentFragment();
        if (parentFragment == null)
            throw new NullPointerException("This is a top-level Fragment");
        return parentFragment;
    }

    /**
     * @return to LayoutInflater (will not leak Activity memory)
     */
    @NonNull
    public final LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(getContext());
    }

    public <T extends View> T findViewById(int i) {
        return mView != null ? mView.findViewById(i) : null;
    }

    /**
     * @return activity的Lifecycle
     */
    @Override
    @NonNull
    public final Lifecycle getLifecycle() {
        return requireActivity().getLifecycle();
    }

    /**
     * @return 获取fragment的ViewLifecycleOwner
     * 此lifecycle才是fragment的生命周期
     * viewModels也由它管理
     */
    @NonNull
    public final FragmentViewLifecycleOwner getViewLifecycleOwner() {
        return mViewLifecycleOwner;
    }

    /**
     * @return ApplicationContext
     */
    public final Context getContext() {
        return requireActivity().getApplicationContext();
    }

    /**
     * @return fragment添加时设置的tag
     */
    public String getTag() {
        return mTag;
    }

    void setTag(String tag) {
        mTag = tag;
    }

    /**
     * @return 获取宿主activity
     */
    @Nullable
    public final FragmentActivity getActivity() {
        return mActivity;
    }

    @NonNull
    public final FragmentActivity requireActivity() {
        if (mActivity == null) {
            throw new NullPointerException("Fragment does not hold activity");
        }
        return mActivity;
    }

    public void startActivity(Intent intent) {
        requireActivity().startActivity(intent);
    }

    public void startActivity(Intent intent, Bundle bundle) {
        requireActivity().startActivity(intent, bundle);
    }

    public boolean isAdded() {
        return mAdded;
    }

    public boolean isDetached() {
        return mDetached;
    }

    public boolean isRemoving() {
        return mRemoving;
    }

    public boolean isInLayout() {
        return mView != null;
    }

    public boolean isResumed() {
        if (mActivity == null)
            return false;
        switch (getLifecycle().event) {
            case ON_START:
            case ON_RESUME:
                return true;
        }
        return false;
    }

    public boolean isVisible() {
        switch (mViewLifecycleOwner.getLifecycle().event) {
            case ON_STOP:
            case ON_PAUSE:
            case ON_CREATE:
            case ON_DESTROY:
                return false;
        }
        return true;
    }

    public boolean isHidden() {
        switch (mViewLifecycleOwner.getLifecycle().event) {
            case ON_STOP:
            case ON_PAUSE:
            case ON_CREATE:
            case ON_DESTROY:
                return true;
        }
        return false;
    }

    public final Bundle getArguments() {
        return mArguments;
    }

    public final void setArguments(Bundle args) {
        if (isAdded()) {
            throw new IllegalStateException("Fragment already added and state has been saved");
        }
        mArguments = args;
    }

    public final View getRoot() {
        return mView;
    }

    protected void onHiddenChanged(boolean hidden) {
        getLifecycleRegistry().markState(hidden ? Lifecycle.Event.ON_STOP : Lifecycle.Event.ON_START);
    }

    @NonNull
    @Contract(pure = true)
    LifecycleRegistry getLifecycleRegistry() {
        return (LifecycleRegistry) mViewLifecycleOwner.getLifecycle();
    }

    @NonNull
    final ViewGroup getViewContainer() {
        ViewGroup viewContainer = requireActivity().findViewById(mLayoutId);
        if (viewContainer == null) {
            throw new IllegalStateException("Can't find parent layout or " + this);
        }
        return viewContainer;
    }

    protected void onAttach(@NonNull Context context) {
    }

    protected void onCreate(Bundle bundle) {
    }

    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        return null;
    }

    protected void onViewCreated(@NonNull View view) {
    }

    protected void onActivityCreated(Bundle bundle) {
    }

    protected void onStart() {
    }

    protected void onResume() {
    }

    protected void onPause() {
    }

    protected void onStop() {
    }

    protected void onDestroyView() {
    }

    protected void onDestroy() {
    }

    protected void onDetach() {
    }

    protected void onMultiWindowModeChanged(boolean z, Configuration configuration) {
    }

    protected void onConfigurationChanged(Configuration configuration) {
    }

}