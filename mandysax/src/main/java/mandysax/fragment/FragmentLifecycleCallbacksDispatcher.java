package mandysax.fragment;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import mandysax.lifecycle.Lifecycle;
import mandysax.lifecycle.ViewTreeLifecycleOwner;

/**
 * Dispatcher for events to {@link Fragment} instances
 */
class FragmentLifecycleCallbacksDispatcher {

    void registerFragmentLifecycleCallbacks(@NonNull Fragment f) {
        f.mAdded = true;
        Fragment parentFragment = f.getParentFragment();
        new FragmentBindParentLifecycleObserver(parentFragment == null ? f.getLifecycle() : parentFragment.getViewLifecycleOwner().getLifecycle(), f);
    }

    void unregisterFragmentLifecycleCallbacks(@NonNull Fragment f) {
        dispatchOnFragmentViewDestroyed(f);
        dispatchOnFragmentDestroyed(f);
        dispatchOnFragmentDetached(f);
        f.mAdded = false;
        f.getLifecycleRegistry().markState(Lifecycle.Event.ON_DESTROY);
    }

    void dispatchOnFragmentAttached(@NonNull Fragment f, @NonNull FragmentActivity activity) {
        f.mActivity = activity;
        f.mDetached = false;
        f.mRemoving = false;
        f.onAttach(activity);
    }

    void dispatchOnFragmentCreated(@NonNull Fragment f,
                                   @Nullable Bundle savedInstanceState) {
        f.setArguments(savedInstanceState);
        f.onCreate(savedInstanceState);
        f.getLifecycleRegistry().markState(Lifecycle.Event.ON_CREATE);
    }

    void dispatchOnFragmentActivityCreated(@NonNull Fragment f,
                                           @Nullable Bundle savedInstanceState) {
        f.onActivityCreated(savedInstanceState);
    }

    void dispatchOnFragmentViewCreated(@NonNull Fragment f, View v) {
        if (v == null) {
            throw new NullPointerException("Unable to create view for this fragment");
        }
        f.mView = v;
        ViewTreeLifecycleOwner.set(v, f.mViewLifecycleOwner);
        f.getViewContainer().addView(v);
        f.onViewCreated(v);
        v.requestApplyInsets();
    }

    void dispatchOnFragmentStarted(@NonNull Fragment f) {
        f.onStart();
    }

    void dispatchOnFragmentResumed(@NonNull Fragment f) {
        f.onResume();
    }

    void dispatchOnFragmentPaused(@NonNull Fragment f) {
        f.onPause();
    }

    void dispatchOnFragmentStopped(@NonNull Fragment f) {
        f.onStop();
    }

    void dispatchOnFragmentViewRemoved(@NonNull Fragment f) {
        if (f.mView != null && f.mView.getParent() != null) {
            ((ViewGroup) f.mView.getParent()).removeView(f.mView);
        }
    }

    void dispatchOnFragmentViewDestroyed(@NonNull Fragment f) {
        f.mRemoving = true;
        f.onDestroyView();
        dispatchOnFragmentViewRemoved(f);
        f.mView = null;
    }

    void dispatchOnFragmentDestroyed(@NonNull Fragment f) {
        f.onDestroy();
    }

    void dispatchOnFragmentDetached(@NonNull Fragment f) {
        f.onDetach();
        f.mActivity = null;
        f.mDetached = true;
    }

}
