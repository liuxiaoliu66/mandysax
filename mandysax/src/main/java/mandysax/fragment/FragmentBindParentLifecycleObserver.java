package mandysax.fragment;

import androidx.annotation.NonNull;

import mandysax.lifecycle.FullLifecycleObserver;
import mandysax.lifecycle.Lifecycle;
import mandysax.lifecycle.LifecycleOwner;

final class FragmentBindParentLifecycleObserver implements FullLifecycleObserver {

    private LifecycleOwner mLifecycleOwner = null;

    private final Fragment mFragment;

    FragmentBindParentLifecycleObserver(@NonNull Lifecycle parentLifecycle, @NonNull Fragment f) {
        mFragment = f;
        f.getViewLifecycleOwner().getLifecycle().addObserver(new FullLifecycleObserver() {

            @Override
            public void onStart(@NonNull LifecycleOwner owner) {
                FullLifecycleObserver.super.onStart(owner);
                parentLifecycle.addObserver(FragmentBindParentLifecycleObserver.this);
            }

            @Override
            public void onDestroy(@NonNull LifecycleOwner owner) {
                FullLifecycleObserver.super.onDestroy(owner);
                remove();
            }
        });
    }

    @Override
    public final void onStateChanged(@NonNull LifecycleOwner source, @NonNull Lifecycle.Event event) {
        FullLifecycleObserver.super.onStateChanged(source, event);
        mLifecycleOwner = source;
    }

    @Override
    public void onStart(@NonNull LifecycleOwner owner) {
        FullLifecycleObserver.super.onStart(owner);
        mFragment.getLifecycleRegistry().markState(Lifecycle.Event.ON_START);
    }

    @Override
    public void onResume(@NonNull LifecycleOwner owner) {
        FullLifecycleObserver.super.onResume(owner);
        mFragment.getLifecycleRegistry().markState(Lifecycle.Event.ON_RESUME);
    }

    @Override
    public void onPause(@NonNull LifecycleOwner owner) {
        FullLifecycleObserver.super.onPause(owner);
        mFragment.getLifecycleRegistry().markState(Lifecycle.Event.ON_PAUSE);
    }

    @Override
    public void onStop(@NonNull LifecycleOwner owner) {
        FullLifecycleObserver.super.onStop(owner);
        mFragment.getLifecycleRegistry().markState(Lifecycle.Event.ON_STOP);
    }

    private void remove() {
        synchronized (this) {
            if (mLifecycleOwner != null) {
                mLifecycleOwner.getLifecycle().removeObserver(this);
            }
        }
    }

}
