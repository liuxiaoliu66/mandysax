package mandysax.fragment;

import androidx.annotation.NonNull;

import mandysax.lifecycle.Lifecycle;
import mandysax.lifecycle.LifecycleObserver;
import mandysax.lifecycle.LifecycleOwner;
import mandysax.lifecycle.LifecycleRegistry;
import mandysax.lifecycle.ViewModelStore;
import mandysax.lifecycle.ViewModelStoreImpl;
import mandysax.lifecycle.ViewModelStoreOwner;

/**
 * @author Huang hao
 */
public final class FragmentViewLifecycleOwner implements LifecycleOwner, ViewModelStoreOwner {

    private final LifecycleRegistry mLifecycle = new LifecycleRegistry(this);
    private final ViewModelStoreImpl mViewModelStore = new ViewModelStoreImpl();

    FragmentViewLifecycleOwner() {
        mLifecycle.addObserver(new LifecycleObserver() {
            @Override
            public void onStateChanged(@NonNull LifecycleOwner source, @NonNull Lifecycle.Event event) {
                LifecycleObserver.super.onStateChanged(source, event);
                if (event == Lifecycle.Event.ON_DESTROY) {
                    mViewModelStore.clear();
                }
            }
        });
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return mLifecycle;
    }

    @Override
    public ViewModelStore getViewModelStore() {
        return mViewModelStore;
    }

}
