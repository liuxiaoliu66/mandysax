package mandysax.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;

import java.util.ArrayList;

import mandysax.lifecycle.Lifecycle;
import mandysax.lifecycle.LifecycleObserver;
import mandysax.lifecycle.LifecycleOwner;

class FragmentStateManager extends FragmentStore {

    final Handler mHandler = new Handler(Looper.getMainLooper());
    FragmentActivity mActivity;
    Bundle mSavedInstanceState;

    private final FragmentLifecycleCallbacksDispatcher mDispatcher = new FragmentLifecycleCallbacksDispatcher();

    /**
     * 添加fragment
     *
     * @param parentFragment 父fragment
     * @param fragment       待添加的fragment
     * @param id             view id
     * @param tag            标记
     */
    void dispatchAdd(Fragment parentFragment, @NonNull Fragment fragment, @IdRes int id, String tag) {
        if (fragment.isAdded())
            throw new RuntimeException(fragment + " has been added");
        if (tag == null) {
            tag = fragment.getClass().getSimpleName();
        }
        boolean repeat = findFragmentByTag(tag) != null;
        StringBuilder tagBuilder = new StringBuilder(tag);
        while (repeat) {
            repeat = findFragmentByTag(tagBuilder.append("+").toString()) != null;
        }
        tag = tagBuilder.toString();
        fragment.setTag(tag);
        fragment.mLayoutId = id;
        mDispatcher.dispatchOnFragmentAttached(fragment, mActivity);
        mDispatcher.dispatchOnFragmentCreated(fragment, fragment.mArguments);
        if (parentFragment == null) {
            addFragment(fragment);
        } else {
            addFragment(parentFragment, fragment);
        }
        if (!fragment.isInLayout() && id != 0) {
            mDispatcher.dispatchOnFragmentViewCreated(
                    fragment, fragment.onCreateView(LayoutInflater.from(mActivity.getApplicationContext()), fragment.getViewContainer())
            );
        }
    }

    /**
     * 显示fragment
     *
     * @param fragment 待显示的fragment
     * @param anim     动画id
     */
    void dispatchShow(@NonNull Fragment fragment, int anim) {
        if (fragment.isDetached()) {
            throw new IllegalStateException("fragment not added");
        }
        if (fragment.getRoot() == null) {
            return;
        }
        fragment.getRoot().setVisibility(View.VISIBLE);
        if (anim != 0) {
            fragment.getRoot().startAnimation(AnimationUtils.loadAnimation(fragment.getContext(), anim));
        }
        fragment.onHiddenChanged(false);
    }

    /**
     * 移除fragment
     *
     * @param fragment 待移除的fragment
     * @param anim     动画id
     */
    void dispatchRemove(@NonNull Fragment fragment, int anim) {
        if (fragment.isInLayout() && anim != 0) {
            Animation exitAnim = AnimationUtils.loadAnimation(fragment.getContext(), anim);
            exitAnim.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation p1) {
                }

                @Override
                public void onAnimationEnd(Animation p1) {
                    removeFragment(fragment);
                }

                @Override
                public void onAnimationRepeat(Animation p1) {
                }
            });
            fragment.getRoot().startAnimation(exitAnim);
        }
        removeFragment(fragment);
    }

    /**
     * 隐藏fragment
     *
     * @param fragment 待隐藏的fragment
     * @param anim     动画id
     */
    void dispatchHide(@NonNull Fragment fragment, int anim) {
        if (fragment.isDetached()) {
            throw new IllegalStateException("fragment not added");
        }
        if (!fragment.isInLayout()) {
            return;
        }
        if (anim == 0) {
            fragment.getRoot().setVisibility(View.GONE);
        } else {
            Animation exitAnim = AnimationUtils.loadAnimation(fragment.getContext(), anim);
            exitAnim.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation p1) {
                }

                @Override
                public void onAnimationEnd(Animation p1) {
                    fragment.getRoot().setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation p1) {
                }
            });
            fragment.getRoot().startAnimation(exitAnim);
        }
        fragment.onHiddenChanged(true);
    }

    /**
     * 切换fragment
     *
     * @param op 操作集合
     */
    void dispatchReplace(Op op) {
        for (Fragment fragment : values()) {
            if (fragment.getRoot() == null) {
                continue;
            }
            if (fragment.getRoot().getParent() == null) {
                continue;
            }
            if (((View) fragment.getRoot().getParent()).getId() == op.id) {
                if (fragment.isHidden()) {
                    continue;
                }
                if (!op.isAddToBackStack) {
                    fragment.getViewLifecycleOwner().getLifecycle().addObserver(new LifecycleObserver() {
                        @Override
                        public void observer(@NonNull Lifecycle.Event event) {
                            if (event != Lifecycle.Event.ON_STOP) {
                                return;
                            }
                            dispatchRemove(fragment, op.popExitAnim);
                            fragment.getViewLifecycleOwner().getLifecycle().removeObserver(this);
                        }
                    });
                } else {
                    if (op.removed == null) {
                        op.removed = new ArrayList<>();
                    }
                    op.removed.add(fragment);
                }
                dispatchHide(fragment, op.exitAnim);
            }
        }
        dispatchAdd(op.parentFragment, op.fragment, op.id, op.tag);
        dispatchShow(op.fragment, op.popEnterAnim);
    }

    void dispatchMultiWindowModeChanged(boolean isInMultiWindowMode, Configuration newConfig) {
        for (Fragment fragment : values()) {
            fragment.onMultiWindowModeChanged(isInMultiWindowMode, newConfig);
        }
    }

    void dispatchConfigurationChanged(Configuration newConfig) {
        for (Fragment fragment : values()) {
            fragment.onConfigurationChanged(newConfig);
        }
    }

    void dispatchResumeFragment() {
        for (Fragment fragment : values()) {
            if (fragment.isInLayout()) {
                Fragment parentFragment = fragment.getParentFragment();
                if (parentFragment != null)
                    if (fragment.getRoot() != null && parentFragment.getRoot() != null) {
                        ViewGroup parent = parentFragment.findViewById(fragment.mLayoutId);
                        if (parent != null) {
                            parent.addView(fragment.getRoot());
                            continue;
                        }
                    }
                if (fragment.getRoot() != null && fragment.mLayoutId != 0) {
                    fragment.getViewContainer().addView(fragment.getRoot());
                }
            }
            mDispatcher.dispatchOnFragmentActivityCreated(fragment, mSavedInstanceState);
        }
    }

    void dispatchAttach(FragmentActivity activity) {
        mActivity = activity;
        for (Fragment fragment : values()) {
            mDispatcher.dispatchOnFragmentAttached(fragment, mActivity);
        }
    }

    void dispatchStart() {
        for (Fragment fragment : values()) {
            mDispatcher.dispatchOnFragmentStarted(fragment);
        }
    }

    void dispatchResume() {
        for (Fragment fragment : values()) {
            mDispatcher.dispatchOnFragmentResumed(fragment);
        }
    }

    void dispatchPause() {
        for (Fragment fragment : values()) {
            mDispatcher.dispatchOnFragmentPaused(fragment);
        }
    }

    void dispatchStop() {
        for (Fragment fragment : values()) {
            mDispatcher.dispatchOnFragmentStopped(fragment);
        }
    }

    void dispatchDestroy() {
        for (Fragment fragment : values()) {
            mDispatcher.dispatchOnFragmentViewRemoved(fragment);
            mDispatcher.dispatchOnFragmentDestroyed(fragment);
            mDispatcher.dispatchOnFragmentDetached(fragment);
        }
        mSavedInstanceState = null;
        mActivity = null;
    }

    @Override
    void add(@NonNull Fragment fragment) {
        super.add(fragment);
        if (fragment.isAdded()) return;
        mDispatcher.registerFragmentLifecycleCallbacks(fragment);
        fragment.getLifecycle().addObserver(new LifecycleObserver() {
            @Override
            public void onStateChanged(@NonNull LifecycleOwner source, @NonNull Lifecycle.Event event) {
                LifecycleObserver.super.onStateChanged(source, event);
                if (event == Lifecycle.Event.ON_START || event == Lifecycle.Event.ON_RESUME) {
                    mDispatcher.dispatchOnFragmentActivityCreated(fragment, mSavedInstanceState);
                    source.getLifecycle().removeObserver(this);
                }
            }
        });
    }

    @Override
    void remove(@NonNull Fragment fragment) {
        super.remove(fragment);
        if (fragment.isDetached()) return;
        mDispatcher.unregisterFragmentLifecycleCallbacks(fragment);
    }

    @Override
    void clear() {
        for (Fragment fragment : values()) {
            mDispatcher.unregisterFragmentLifecycleCallbacks(fragment);
        }
        super.clear();
    }

    /**
     * @author Huang hao
     */
    static final class Op {
        int id;
        String tag;
        FragmentController.STACK stack;
        Fragment parentFragment;
        Fragment fragment;
        int enterAnim;
        int exitAnim;
        int popEnterAnim;
        int popExitAnim;
        boolean isAddToBackStack;
        ArrayList<Fragment> removed;
    }

}
