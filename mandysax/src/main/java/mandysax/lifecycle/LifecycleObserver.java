package mandysax.lifecycle;

import androidx.annotation.NonNull;

/**
 * @author Huang hao
 */
public interface LifecycleObserver {
    /**
     * Called when event changes
     *
     * @param event The event
     */
    default void observer(@NonNull Lifecycle.Event event) {
    }

    /**
     * Called when a state transition event happens.
     *
     * @param source The source of the event
     * @param event  The event
     */
    default void onStateChanged(@NonNull LifecycleOwner source, @NonNull Lifecycle.Event event) {
    }
}
