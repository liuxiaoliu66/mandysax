package mandysax.lifecycle;

import android.util.ArrayMap;

import androidx.annotation.Nullable;

/**
 * @author Huang hao
 */
public final class ViewModelStoreImpl implements ViewModelStore {
    private final ArrayMap<Class<?>, ViewModel> mMap = new ArrayMap<>();

    /**
     * @param key       Extends the ViewModel class
     * @param viewModel put in ViewModelStore ViewModel object
     */
    @Override
    public void put(Class<?> key, ViewModel viewModel) {
        mMap.put(key, viewModel);
    }

    /**
     * @param key Extends the ViewModel class
     * @return Your ViewModel object
     */
    @Nullable
    @Override
    public ViewModel get(Class<?> key) {
        ViewModel viewModel = mMap.get(key);
        if (viewModel != null) {
            if (key.isInstance(viewModel)) {
                return viewModel;
            }
        }
        return null;
    }

    /**
     * clear ViewModelStore
     */
    @Override
    public void clear() {
        for (ViewModel vm : mMap.values()) {
            vm.clear();
        }
        mMap.clear();
    }

}
