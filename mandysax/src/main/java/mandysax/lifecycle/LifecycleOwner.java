package mandysax.lifecycle;

import androidx.annotation.NonNull;

/**
 * @author Huang hao
 */
public interface LifecycleOwner {
    /**
     * 获取托管的lifecycle
     *
     * @return lifecycle
     */
    @NonNull
    Lifecycle getLifecycle();
}
