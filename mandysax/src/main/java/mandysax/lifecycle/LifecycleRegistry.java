package mandysax.lifecycle;

import androidx.annotation.NonNull;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Huang hao
 */
public final class LifecycleRegistry extends Lifecycle {

    private final CopyOnWriteArrayList<LifecycleObserver> mObservers = new CopyOnWriteArrayList<>();

    private final LifecycleOwner mOwner;

    public LifecycleRegistry() {
        mOwner = null;
    }

    public LifecycleRegistry(LifecycleOwner owner) {
        mOwner = owner;
    }

    @Override
    public void addObserver(@NonNull LifecycleObserver observer) {
        if (event == Event.ON_DESTROY) {
            return;
        }
        if (mObservers.contains(observer))
            return;
        mObservers.add(observer);
        if (event != null) {
            if (mOwner != null)
                observer.onStateChanged(mOwner, event);
            observer.observer(event);
        }
    }

    @Override
    public void removeObserver(@NonNull LifecycleObserver observer) {
        if (event == Event.ON_DESTROY) {
            return;
        }
        mObservers.remove(observer);
    }

    public void markState(Lifecycle.Event state) {
        if (event == state)
            return;
        event = state;
        for (LifecycleObserver observer : mObservers) {
            if (mOwner != null)
                observer.onStateChanged(mOwner, state);
            observer.observer(state);
        }
        if (state == Event.ON_DESTROY) {
            mObservers.clear();
        }
    }

}

