package mandysax.lifecycle;

/**
 * @author Huang hao
 */
public interface ViewModelStoreOwner {
    ViewModelStore getViewModelStore();
}
