package mandysax.lifecycle.livedata;

/**
 * @author Huang hao
 */
public interface Observer<T> {
    /**
     * will be called when value changes
     *
     * @param p1 newValue
     */
    void onChanged(T p1);
}
