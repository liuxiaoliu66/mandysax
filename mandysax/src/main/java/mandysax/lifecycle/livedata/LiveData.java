package mandysax.lifecycle.livedata;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.CopyOnWriteArrayList;

import mandysax.lifecycle.Lifecycle;
import mandysax.lifecycle.LifecycleObserver;
import mandysax.lifecycle.LifecycleOwner;

/**
 * @author Huang hao
 */
public class LiveData<T> {

    volatile T mValue;

    private static Handler sHandler;

    private final CopyOnWriteArrayList<ObserverWrapper> mObservers = new CopyOnWriteArrayList<>();

    void postValue(T value) {
        if (isMainThread()) {
            setValue(value);
            return;
        }
        if (sHandler == null) {
            synchronized (LiveData.class) {
                if (sHandler == null) {
                    sHandler = new Handler(Looper.getMainLooper());
                }
            }
        }
        sHandler.post(() -> setValue(value));
    }

    void setValue(T value) {
        if (!isMainThread()) {
            throw new IllegalStateException("Cannot invoke setValue on a background thread");
        }
        mValue = value;
        start();
    }

    private boolean isMainThread() {
        return Looper.getMainLooper() == Looper.myLooper();
    }

    /**
     * @return LIveData托管的value
     */
    public T getValue() {
        return mValue;
    }

    /**
     * @param lifecycleOwner 生命周期所有者
     * @param observer       观察者
     */
    public void lazy(@NonNull LifecycleOwner lifecycleOwner, @NonNull Observer<? super T> observer) {
        if (lifecycleOwner.getLifecycle().event == Lifecycle.Event.ON_DESTROY) {
            return;
        }
        if (mValue != null) {
            observer.onChanged(mValue);
            return;
        }
        observe(lifecycleOwner, new Observer<T>() {
            @Override
            public void onChanged(T p1) {
                if (mValue != null) {
                    observer.onChanged(mValue);
                    removeObserver(this);
                }
            }
        });
    }

    /**
     * @param observer 观察者
     */
    public void lazy(Observer<? super T> observer) {
        if (mValue != null) {
            observer.onChanged(mValue);
            return;
        }
        observeForever(new Observer<T>() {
            @Override
            public void onChanged(T p2) {
                if (p2 != null) {
                    observer.onChanged(p2);
                    removeObserver(this);
                }
            }
        });
    }

    /**
     * @param observer 观察者
     */
    public void observeForever(Observer<? super T> observer) {
        Observer<? super T> existing = ifAbsent(observer);
        if (existing != null) {
            return;
        }
        mObservers.add(new AlwaysActiveObserver(observer));
        if (mValue != null) {
            observer.onChanged(mValue);
        }
    }

    /**
     * @param owner    生命周期所有者
     * @param observer 观察者
     */
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<? super T> observer) {
        if (owner.getLifecycle().event == Lifecycle.Event.ON_DESTROY) {
            return;
        }
        Observer<? super T> existing = ifAbsent(observer);
        if (existing != null) {
            return;
        }
        mObservers.add(new LifecycleBoundObserver(owner, observer));
        if (mValue != null) {
            observer.onChanged(mValue);
        }
    }

    @Nullable
    private Observer<? super T> ifAbsent(Observer<? super T> observer) {
        for (ObserverWrapper wrapper : mObservers) {
            if (wrapper.mObserver.equals(observer)) {
                return observer;
            }
        }
        return null;
    }

    /**
     * @param observer 需要移除的观察者
     */
    public void removeObserver(Observer<? super T> observer) {
        for (ObserverWrapper wrapper : mObservers) {
            if (wrapper.mObserver.equals(observer)) {
                mObservers.remove(wrapper);
                return;
            }
        }
    }

    /**
     * 向Observer推送Value的新状态
     */
    private void start() {
        for (ObserverWrapper wrapper : mObservers) {
            if (wrapper.isActive()) {
                wrapper.mObserver.onChanged(mValue);
            } else if (wrapper instanceof LiveData.LifecycleBoundObserver) {
                LiveData<T>.LifecycleBoundObserver boundObserver = ((LiveData<T>.LifecycleBoundObserver) wrapper);
                boundObserver.sync();
            }
        }
    }

    private abstract class ObserverWrapper {
        protected final Observer<? super T> mObserver;

        ObserverWrapper(Observer<? super T> mObserver) {
            this.mObserver = mObserver;
        }

        protected abstract boolean isActive();

        protected abstract void sync();
    }

    private final class LifecycleBoundObserver extends ObserverWrapper implements LifecycleObserver {

        private final LifecycleOwner mOwner;

        private volatile boolean mActive = false;

        private volatile boolean mSync = false;

        LifecycleBoundObserver(@NonNull LifecycleOwner owner, Observer<? super T> observer) {
            super(observer);
            mOwner = owner;
            owner.getLifecycle().addObserver(this);
        }

        @Override
        public void observer(@NonNull Lifecycle.Event event) {
            switch (event) {
                case ON_PAUSE:
                case ON_STOP:
                    mActive = false;
                    break;
                case ON_DESTROY:
                    mActive = false;
                    mOwner.getLifecycle().removeObserver(this);
                    mObservers.remove(this);
                    break;
                default:
                    mActive = true;
                    if (mSync) {
                        mObserver.onChanged(mValue);
                        mSync = false;
                    }
                    break;
            }
        }

        @Override
        protected boolean isActive() {
            return mActive;
        }

        @Override
        protected void sync() {
            mSync = true;
        }
    }

    private final class AlwaysActiveObserver extends ObserverWrapper {

        AlwaysActiveObserver(Observer<? super T> observer) {
            super(observer);
        }

        @Override
        protected boolean isActive() {
            return true;
        }

        @Override
        protected void sync() {
        }

    }

}

