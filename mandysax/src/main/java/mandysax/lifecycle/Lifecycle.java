package mandysax.lifecycle;

/**
 * @author Huang hao
 */
public abstract class Lifecycle {

    public volatile Lifecycle.Event event;

    public abstract void addObserver(LifecycleObserver observer);

    public abstract void removeObserver(LifecycleObserver observer);

    public enum Event {
        ON_CREATE(),
        ON_START(),
        ON_RESUME(),
        ON_PAUSE(),
        ON_STOP(),
        ON_DESTROY()
    }
}
