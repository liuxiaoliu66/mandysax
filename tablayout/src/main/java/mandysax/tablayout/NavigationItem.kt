package mandysax.tablayout

import android.view.View
import androidx.annotation.ColorInt
import mandysax.tablayout.databinding.NavigactionItemBinding

class NavigationItem(
    private val text: String
) : TabItemModel {
    @ColorInt
    private var mActiveColor: Int? = null

    @ColorInt
    private var mInActiveColor: Int? = null

    override fun setActiveColor(@ColorInt color: Int): NavigationItem {
        mActiveColor = color
        return this
    }

    override fun setInActiveColor(@ColorInt color: Int): NavigationItem {
        mInActiveColor = color
        return this
    }

    override fun getLayoutId(): Int = R.layout.navigaction_item

    override fun active(itemView: View, active: Boolean) {
        val binding = NavigactionItemBinding.bind(itemView)
        binding.text.text = text
        when (active) {
            true ->
                mActiveColor
            false ->
                mInActiveColor
        }?.also {
            binding.text.setTextColor(it)
        }
        (if (active) mActiveColor else android.R.color.transparent)?.let {
            binding.view.setBackgroundColor(
                it
            )
        }
    }

}