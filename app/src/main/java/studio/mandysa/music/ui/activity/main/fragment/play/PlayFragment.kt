package studio.mandysa.music.ui.activity.main.fragment.play

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.RelativeLayout
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.flaviofaria.kenburnsview.RandomTransitionGenerator
import mandysax.fragment.Fragment
import mandysax.tablayout.BottomNavigationItem
import mandysax.tablayout.setActiveColor
import mandysax.tablayout.setInActiveColor
import mandysax.viewpager2.adapter.FragmentStateAdapter
import studio.mandysa.music.R
import studio.mandysa.music.databinding.FragmentPlayBinding
import studio.mandysa.music.logic.ktx.playManager
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.service.playmanager.PlayManager


class PlayFragment : Fragment() {

    private val mBinding: FragmentPlayBinding by viewBinding()

    private fun View.setMargins(left: Int, top: Int, right: Int, bottom: Int) {
        val lp = layoutParams as RelativeLayout.LayoutParams
        lp.setMargins(left, top, right, bottom)
        layoutParams = lp
    }

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        ViewCompat.setOnApplyWindowInsetsListener(
            view
        ) { _, insets ->
            val startBarSize = insets!!.getInsets(WindowInsetsCompat.Type.statusBars()).top
            val navigationBarSize =
                insets.getInsets(WindowInsetsCompat.Type.navigationBars()).bottom
            mBinding.playViewPager.setMargins(0, startBarSize, 0, 0)
            mBinding.navigationBarDivider.setMargins(0, 0, 0, navigationBarSize)
            insets
        }
        mBinding.root.setOnClickListener { }
        mBinding.playViewPager.isUserInputEnabled = false
        mBinding.playViewPager.adapter = object : FragmentStateAdapter() {

            private val list = listOf(
                PlayingFragment(), LyricFragment(), PlayQueueFragment()
            )

            override fun createFragment(position: Int): Fragment {
                return list[position]
            }

            override fun getItemCount(): Int {
                return list.size
            }

        }
        mBinding.playBottomNavigationBar.apply {
            models = listOf(
                BottomNavigationItem(
                    R.drawable.ic_round_music_video_24,
                    ""
                ),
                BottomNavigationItem(
                    R.drawable.ic_round_translate_24,
                    ""
                ), BottomNavigationItem(
                    R.drawable.ic_round_format_list_bulleted_24,
                    ""
                )
            )
                .setInActiveColor(context.getColor(R.color.translucent_white))
                .setActiveColor(context.getColor(android.R.color.white))
            getSelectedPosition().observe(viewLifecycleOwner) {
                mBinding.playViewPager.setCurrentItem(it, true)
            }
        }
        playManager {
            playStateLivedata().observe(viewLifecycleOwner) {
                if (it == PlayManager.STATE.PAUSE) {
                    mBinding.playBackground.pause()
                } else {
                    mBinding.playBackground.resume()
                }
            }
            changeMusicLiveData()
                .observe(viewLifecycleOwner) { model ->
                    mBinding.playBackground.load(model.coverUrl)
                }
        }
        //背景模糊动画速度
        mBinding.playBackground.setTransitionGenerator(
            RandomTransitionGenerator(
                2000,
                LinearInterpolator()
            )
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }

}