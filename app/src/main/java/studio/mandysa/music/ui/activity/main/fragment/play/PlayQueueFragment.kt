package studio.mandysa.music.ui.activity.main.fragment.play

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mandysax.fragment.Fragment
import studio.mandysa.jiuwo.utils.linear
import studio.mandysa.jiuwo.utils.recyclerAdapter
import studio.mandysa.jiuwo.utils.setup
import studio.mandysa.music.R
import studio.mandysa.music.databinding.FragmentPlayQueueBinding
import studio.mandysa.music.databinding.ItemSongMiniBinding
import studio.mandysa.music.logic.ktx.load
import studio.mandysa.music.logic.ktx.playManager
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.model.AlbumModel
import studio.mandysa.music.logic.model.MusicModel
import studio.mandysa.music.logic.model.RecommendSongs
import studio.mandysa.music.logic.model.SingerModel
import studio.mandysa.music.service.playmanager.PlayManager
import studio.mandysa.music.service.playmanager.ktx.allArtist

class PlayQueueFragment : Fragment() {

    private val mBinding: FragmentPlayQueueBinding by viewBinding()

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        mBinding.recycler.linear().setup {
            addType<MusicModel>(R.layout.item_song_mini)
            addType<RecommendSongs.RecommendSong>(R.layout.item_song_mini)
            onBind {
                val model =
                    getModel<studio.mandysa.music.service.playmanager.model.MusicModel<SingerModel, AlbumModel>>()
                ItemSongMiniBinding.bind(itemView).apply {
                    songIndex.text = (modelPosition + 1).toString()
                    songName.text = model.title
                    songSingerName.text = model.artist.allArtist()
                    itemView.setOnClickListener {
                        PlayManager.loadPlaylist(
                            models,
                            modelPosition
                        )
                    }
                }
            }
        }
        mBinding.toolbar.more.visibility = View.GONE
        mBinding.clear.setOnClickListener {
            mBinding.recycler.recyclerAdapter.clearModels()
        }
        playManager {
            changeMusicLiveData()
                .observe(viewLifecycleOwner) { model ->
                    mBinding.toolbar.songName.text = model.title
                    mBinding.toolbar.songSingerName.text = model.artist.allArtist()
                    mBinding.toolbar.songCover.load(
                        model.coverUrl
                    )
                }
            changePlayListLiveData().observe(viewLifecycleOwner) {
                mBinding.recycler.recyclerAdapter.models = it
                mBinding.index.text = it?.size.toString()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }
}