package studio.mandysa.music.ui.activity.main.fragment.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.sothree.slidinguppanel.PanelSlideListener
import com.sothree.slidinguppanel.PanelState
import com.yanzhenjie.sofia.Sofia
import mandysax.core.app.OnBackPressedCallback
import mandysax.fragment.Fragment
import mandysax.fragment.activityViewModels
import mandysax.tablayout.BottomNavigationItem
import mandysax.tablayout.setActiveColor
import mandysax.tablayout.setInActiveColor
import mandysax.viewpager2.adapter.FragmentStateAdapter
import studio.mandysa.music.R
import studio.mandysa.music.databinding.FragmentHomeBinding
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.service.playmanager.PlayManager
import studio.mandysa.music.ui.activity.main.fragment.browse.BrowseFragment
import studio.mandysa.music.ui.activity.main.fragment.me.MeFragment
import studio.mandysa.music.ui.activity.main.fragment.search.SearchFragment
import studio.mandysa.music.ui.activity.main.helper.NavFragmentHelper
import studio.mandysa.music.ui.activity.main.helper.ViewPagerScrollPositionHelper
import studio.mandysa.music.ui.event.NavigationViewModel

class HomeFragment : Fragment() {
    private val mBinding by viewBinding<FragmentHomeBinding>()

    private val mNavigationViewModel by activityViewModels<NavigationViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return mBinding.root
    }

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        mBinding.let { it ->
            requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,
                object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {
                        when (mBinding.homeSlidingView.panelState) {
                            PanelState.EXPANDED, PanelState.DRAGGING -> {
                                mBinding.homeSlidingView.panelState = PanelState.COLLAPSED
                            }
                            else -> {
                                requireActivity().finish()
                            }
                        }
                    }
                })
            mNavigationViewModel.fragmentLiveData.observe(viewLifecycleOwner) { fragment ->
                fragment ?: return@observe
                if (it.homeSlidingView.panelState == PanelState.EXPANDED)
                    it.homeSlidingView.panelState = PanelState.COLLAPSED
            }
            it.homeSlidingView.scrollableViewHelper.setPositionHelpers(
                mutableListOf(
                    ViewPagerScrollPositionHelper()
                )
            )
            it.homeSlidingView.setEnabledScrollable(true)
            it.controllerFragment.setOnClickListener {
                mBinding.homeSlidingView.panelState = PanelState.EXPANDED
            }
            it.homeBottomNavLayout.setOnClickListener { }
            it.homeViewPager.isUserInputEnabled = false
            it.homeViewPager.adapter = object : FragmentStateAdapter() {

                val list = listOf(
                    NavFragmentHelper.create(BrowseFragment()),
                    NavFragmentHelper.create(SearchFragment()),
                    NavFragmentHelper.create(MeFragment())
                )

                override fun createFragment(position: Int): Fragment {
                    return list[position]
                }

                override fun getItemCount(): Int {
                    return list.size
                }

            }
            it.homeBottomNavigationBar.models = listOf(
                BottomNavigationItem(
                    R.drawable.ic_round_contactless_24,
                    context.getString(R.string.browse)
                ),
                BottomNavigationItem(
                    R.drawable.ic_round_search_24,
                    context.getString(R.string.search)
                ),
                BottomNavigationItem(
                    R.drawable.ic_round_account_circle_24,
                    context.getString(R.string.me)
                )
            ).setActiveColor(context.getColor(R.color.theme_color))
                .setInActiveColor(context.getColor(mandysax.tablayout.R.color.default_unchecked_color))
            it.homeBottomNavigationBar.getSelectedPosition().observe(viewLifecycleOwner) { pos ->
                it.homeViewPager.setCurrentItem(pos, false)
            }
            ViewCompat.setOnApplyWindowInsetsListener(it.root) { _, insets ->
                val startBarSize = insets.getInsets(WindowInsetsCompat.Type.statusBars()).top
                it.statusBarBlurBackground.layoutParams.height = startBarSize
                val navigationBarSize =
                    insets.getInsets(WindowInsetsCompat.Type.navigationBars()).bottom
                it.homeBottomNavLayout.layoutParams.height =
                    context.resources.getDimensionPixelSize(
                        R.dimen.nav_height
                    ) + navigationBarSize
                PlayManager.changeMusicLiveData()
                    .lazy(viewLifecycleOwner) { _ ->
                        it.homeSlidingView.panelHeight =
                            context.resources.getDimensionPixelSize(R.dimen.controller_height) + context.resources.getDimensionPixelSize(
                                R.dimen.nav_height
                            ) + navigationBarSize
                        val y =
                            mBinding.root.bottom - (context.resources.getDimensionPixelSize(R.dimen.nav_height) + navigationBarSize)
                        object : PanelSlideListener {
                            override fun onPanelSlide(panel: View, slideOffset: Float) {
                                val by: Float =
                                    y + it.homeBottomNavLayout.height * slideOffset * 8
                                if (by < y) return
                                it.homeBottomNavLayout.y = by
                                val alpha = slideOffset * 40
                                it.playFragment.alpha = alpha
                                it.controllerFragment.alpha = (1 - alpha).also {
                                    mBinding.controllerFragment.visibility =
                                        if (it > 0f) View.VISIBLE else View.GONE
                                }
                            }

                            override fun onPanelStateChanged(
                                panel: View,
                                previousState: PanelState,
                                newState: PanelState
                            ) {
                                when (newState) {
                                    PanelState.EXPANDED -> {
                                        Sofia.with(requireActivity())
                                            .statusBarLightFont()
                                    }
                                    PanelState.DRAGGING -> {
                                        if (mBinding.playBackground.background == null)
                                            mBinding.playBackground.background =
                                                context.getDrawable(android.R.color.white)
                                    }
                                    else -> {
                                        mBinding.playBackground.background = null
                                        Sofia.with(requireActivity())
                                            .statusBarDarkFont()
                                    }
                                }
                            }
                        }.also { listener ->
                            if (it.homeSlidingView.panelSlideListenerList.size == 0) {
                                it.homeSlidingView.panelSlideListenerList.add(listener)
                            } else it.homeSlidingView.panelSlideListenerList[0] = listener
                        }
                    }
                insets
            }
        }
    }
}