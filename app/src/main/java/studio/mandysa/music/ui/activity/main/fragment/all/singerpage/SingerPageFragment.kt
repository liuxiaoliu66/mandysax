package studio.mandysa.music.ui.activity.main.fragment.all.singerpage

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView.HORIZONTAL
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mandysax.fragment.Fragment
import mandysax.lifecycle.coroutineScope
import studio.mandysa.jiuwo.utils.linear
import studio.mandysa.jiuwo.utils.recyclerAdapter
import studio.mandysa.jiuwo.utils.setup
import studio.mandysa.jiuwo.utils.staggered
import studio.mandysa.music.R
import studio.mandysa.music.databinding.*
import studio.mandysa.music.logic.ktx.load
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.model.MusicModel
import studio.mandysa.music.logic.model.NeteaseCloudMusicApi
import studio.mandysa.music.logic.model.SingerDetailedModel
import studio.mandysa.music.logic.model.SingerHotSongModel
import studio.mandysa.music.logic.model.design.SubTitleModel
import studio.mandysa.music.logic.network.ServiceCreator
import studio.mandysa.music.service.playmanager.PlayManager
import studio.mandysa.music.service.playmanager.ktx.allArtist
import studio.mandysa.music.ui.activity.main.fragment.all.songmenu.SongMenuFragment

class SingerPageFragment(private val mId: String) : Fragment() {

    private val mBinding: LayoutStateRvBinding by viewBinding()

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        mBinding.apply {
            ViewCompat.setOnApplyWindowInsetsListener(root) { _, insets ->
                val startBarSize = insets.getInsets(WindowInsetsCompat.Type.statusBars()).top
                val navigationBarSize =
                    insets.getInsets(WindowInsetsCompat.Type.navigationBars()).bottom
                recycler.setPadding(
                    0,
                    startBarSize,
                    0,
                    context.resources.getDimensionPixelSize(R.dimen.controller_height) + context.resources.getDimensionPixelSize(
                        R.dimen.nav_height
                    ) + navigationBarSize
                )
                insets
            }
            recycler.linear().setup {
                addType<SingerDetailedModel>(R.layout.item_singer_header) {
                    val model = getModel<SingerDetailedModel>()
                    ItemSingerHeaderBinding.bind(itemView).apply {
                        singerName.text = model.name
                        singerAvatar.load(model.cover)
                    }
                }
                addType<SubTitleModel>(R.layout.item_sub_title) {
                    ItemSubTitleBinding.bind(itemView).title.text =
                        getModel<SubTitleModel>().title
                }
                val pagerSnapHelper = PagerSnapHelper()
                addType<SingerHotSongModel>(R.layout.layout_rv) {
                    LayoutRvBinding.bind(itemView).recycler.apply {
                        pagerSnapHelper.attachToRecyclerView(this)
                        staggered(5, orientation = HORIZONTAL).setup {
                            addType<MusicModel>(R.layout.item_song) {
                                ItemSongBinding.bind(itemView).apply {
                                    val model = getModel<MusicModel>()
                                    songIndex.text = (modelPosition + 1).toString()
                                    songName.text = model.title
                                    songSingerName.text = model.artist.allArtist()
                                    itemView.setOnClickListener {
                                        PlayManager.loadPlaylist(
                                            models,
                                            modelPosition
                                        )
                                    }
                                    more.setOnClickListener {
                                        SongMenuFragment(model).show(childFragmentManager)
                                    }
                                }
                            }
                        }
                        recyclerAdapter.models = getModel<SingerHotSongModel>().list
                    }
                }
            }
            stateLayout.showLoading {
                viewLifecycleOwner.lifecycle.coroutineScope.launch(Dispatchers.IO) {
                    val singerHeader = ServiceCreator.create(NeteaseCloudMusicApi::class.java)
                        .getSingerDetails(mId).execute()
                    val singerHotSongModel = ServiceCreator.create(NeteaseCloudMusicApi::class.java)
                        .getSingerHotSong(mId).execute()
                    withContext(Dispatchers.Main) {
                        recycler.recyclerAdapter.models =
                            listOf(singerHeader, SubTitleModel("热门歌曲"), singerHotSongModel)
                        stateLayout.showContentState()
                    }
                }
            }
            stateLayout.showLoadingState()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }
}