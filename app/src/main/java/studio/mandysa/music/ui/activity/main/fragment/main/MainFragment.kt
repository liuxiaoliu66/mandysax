package studio.mandysa.music.ui.activity.main.fragment.main

import android.view.View
import mandysax.fragment.activityViewModels
import mandysax.navigation.fragment.NavHostFragment
import studio.mandysa.music.ui.activity.main.fragment.home.HomeFragment
import studio.mandysa.music.ui.activity.main.fragment.start.StartFragment
import studio.mandysa.music.ui.event.EventViewModel

class MainFragment : NavHostFragment() {

    private val mEventViewModel by activityViewModels<EventViewModel>()

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        navController.navigate(HomeFragment())
        if (mEventViewModel.isNotLogin()) {
            navController.navigate(StartFragment())
        }
    }
}