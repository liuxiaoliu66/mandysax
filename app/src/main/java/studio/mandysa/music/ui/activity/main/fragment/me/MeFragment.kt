package studio.mandysa.music.ui.activity.main.fragment.me

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView.HORIZONTAL
import mandysax.fragment.Fragment
import mandysax.fragment.activityViewModels
import mandysax.fragment.viewModels
import mandysax.navigation.Navigation
import studio.mandysa.jiuwo.utils.linear
import studio.mandysa.jiuwo.utils.recyclerAdapter
import studio.mandysa.jiuwo.utils.setup
import studio.mandysa.music.R
import studio.mandysa.music.databinding.*
import studio.mandysa.music.logic.decoration.HorizontalAlbumItemDecoration
import studio.mandysa.music.logic.ktx.load
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.model.ListModel
import studio.mandysa.music.logic.model.UserModel
import studio.mandysa.music.logic.model.UserPlaylistModel
import studio.mandysa.music.logic.model.design.SubTitleModel
import studio.mandysa.music.logic.model.design.TitleModel
import studio.mandysa.music.ui.activity.main.fragment.all.playlist.PlaylistFragment
import studio.mandysa.music.ui.activity.main.fragment.me.user.UserFragment
import studio.mandysa.music.ui.event.EventViewModel

class MeFragment : Fragment() {

    private val mBinding: LayoutStateRvBinding by viewBinding()

    private val mEvent: EventViewModel by activityViewModels()

    private val mViewModel by viewModels<MeViewModel>()

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        ViewCompat.setOnApplyWindowInsetsListener(root) { _, insets ->
            val navigationBarSize =
                insets.getInsets(WindowInsetsCompat.Type.navigationBars()).bottom
            mBinding.recycler.setPadding(
                0,
                0,
                0,
                context.resources.getDimensionPixelSize(R.dimen.controller_height) + context.resources.getDimensionPixelSize(
                    R.dimen.nav_height
                ) + navigationBarSize
            )
            insets
        }
        mBinding.recycler.linear().setup {
            addType<TitleModel>(R.layout.item_title) {
                ItemTitleBinding.bind(itemView).titleTv.text =
                    getModel<TitleModel>().title
            }
            addType<SubTitleModel>(R.layout.item_sub_title) {
                ItemSubTitleBinding.bind(itemView).title.text =
                    getModel<SubTitleModel>().title
            }
            addType<UserModel>(R.layout.item_user) {
                val model = getModel<UserModel>()
                ItemUserBinding.bind(itemView).apply {
                    ivAvatar.load(model.avatarUrl)
                    tvNickname.text = model.nickname
                    tvSignature.text = model.signature
                    itemView.setOnClickListener {
                        UserFragment().show(childFragmentManager)
                    }
                }
            }
            addType<UserPlaylistModel>(R.layout.layout_rv)
            addType<ListModel>(R.layout.layout_rv)
            val linearSnapHelper = LinearSnapHelper()
            onBind {
                when (mModel) {
                    is UserPlaylistModel -> {
                        LayoutRvBinding.bind(itemView).recycler.apply {
                            linearSnapHelper.attachToRecyclerView(this)
                            if (itemDecorationCount == 0)
                                addItemDecoration(HorizontalAlbumItemDecoration())
                            linear(orientation = HORIZONTAL).setup {
                                addType<UserPlaylistModel.UserPlaylist>(R.layout.item_playlist)
                                onBind {
                                    val model = getModel<UserPlaylistModel.UserPlaylist>()
                                    ItemPlaylistBinding.bind(itemView).apply {
                                        playlistCover.load(
                                            model.coverImgUrl
                                        )
                                        playlistTitle.text = model.name
                                        itemView.setOnClickListener {
                                            Navigation.findViewNavController(it)
                                                .navigate(
                                                    PlaylistFragment(
                                                        model.id!!
                                                    )
                                                )
                                        }
                                    }
                                }
                            }
                            recyclerAdapter.models = getModel<UserPlaylistModel>().list
                        }
                    }
                    is ListModel -> {
                        LayoutRvBinding.bind(itemView).recycler.apply {
                            linearSnapHelper.attachToRecyclerView(this)
                            if (itemDecorationCount == 0)
                                addItemDecoration(HorizontalAlbumItemDecoration())
                            linear(orientation = HORIZONTAL).setup {
                                addType<ListModel.Model>(R.layout.item_playlist)
                                onBind {
                                    val model = getModel<ListModel.Model>()
                                    ItemPlaylistBinding.bind(itemView).apply {
                                        playlistCover.load(
                                            model.coverImgUrl
                                        )
                                        playlistTitle.text = model.name
                                        itemView.setOnClickListener {
                                            Navigation.findViewNavController(it)
                                                .navigate(
                                                    PlaylistFragment(
                                                        model.id
                                                    )
                                                )
                                        }
                                    }
                                }
                            }
                            recyclerAdapter.models = getModel<ListModel>().list
                        }
                    }
                    else -> {}
                }
            }
        }
        mBinding.stateLayout.showLoading {
            mEvent.getCookieLiveData().lazy { it1 ->
                mViewModel.bind(context, it1).lazy {
                    when (it) {
                        is MeViewModel.Status.Error -> mBinding.stateLayout.showErrorState()
                        is MeViewModel.Status.Next -> {
                            mBinding.recycler.recyclerAdapter.models = it.value
                            mBinding.stateLayout.showContentState()
                        }
                    }
                }
            }
        }
        mBinding.stateLayout.showError {
            mBinding.recycler.recyclerAdapter.clearModels()
        }
        mBinding.stateLayout.showLoadingState()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }

}