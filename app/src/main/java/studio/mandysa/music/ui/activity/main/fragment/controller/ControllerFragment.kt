package studio.mandysa.music.ui.activity.main.fragment.controller

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mandysax.fragment.Fragment
import studio.mandysa.music.R
import studio.mandysa.music.databinding.FragmentControllerBinding
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.ktx.load
import studio.mandysa.music.logic.ktx.playManager

class ControllerFragment : Fragment() {

    private val mBinding: FragmentControllerBinding by viewBinding()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        playManager {
            changeMusicLiveData().observe(viewLifecycleOwner) {
                mBinding.apply {
                    controllerTitle.text = it.title
                    controllerCover.load(it.coverUrl)
                }
            }
            pauseLiveData().observe(viewLifecycleOwner) {
                mBinding.controllerPlayOrPause.setImageResource(if (it) R.drawable.ic_play else R.drawable.ic_pause)
            }
            mBinding.controllerPlayOrPause.setOnClickListener {
                if (pauseLiveData().value == true)
                    play()
                else pause()
            }
            mBinding.controllerNextSong.setOnClickListener {
                skipToNext()
            }
        }
    }

}