package studio.mandysa.music.ui.activity.main.fragment.search.realsearch

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import mandysax.fragment.Fragment
import mandysax.lifecycle.livedata.MutableLiveData
import mandysax.tablayout.NavigationItem
import mandysax.tablayout.setActiveColor
import mandysax.tablayout.setInActiveColor
import mandysax.viewpager2.adapter.FragmentStateAdapter
import studio.mandysa.music.R
import studio.mandysa.music.databinding.FragmentRealSearchBinding
import studio.mandysa.music.logic.ktx.hideInput
import studio.mandysa.music.logic.ktx.showInput
import studio.mandysa.music.logic.ktx.viewBinding

class RealSearchFragment : Fragment() {

    private val mBinding: FragmentRealSearchBinding by viewBinding()

    private val mSearchContentLiveData = MutableLiveData<String>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        ViewCompat.setOnApplyWindowInsetsListener(root) { _, insets ->
            val startBarSize = insets.getInsets(WindowInsetsCompat.Type.statusBars()).top
            root.setPadding(
                0,
                startBarSize,
                0,
                0
            )
            insets
        }
        mBinding.apply {
            search.searchEt.setTextIsSelectable(true)
            search.searchEt.isFocusable = true
            search.searchEt.isCursorVisible = true
            search.searchEt.showInput()
            search.searchEt.setOnEditorActionListener { var1, i, _ ->
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    hideInput()
                    mSearchContentLiveData.value = var1.text.toString()
                }
                false
            }
            searchViewPager.isUserInputEnabled = false
            searchViewPager.adapter = object : FragmentStateAdapter() {
                private val list = listOf(
                    RealSearchMusicFragment(mSearchContentLiveData),
                    RealSearchSingerFragment(mSearchContentLiveData),
                    RealSearchPlaylistFragment(mSearchContentLiveData)
                )

                override fun createFragment(position: Int): Fragment {
                    return list[position]
                }

                override fun getItemCount(): Int {
                    return list.size
                }

            }
            searchTabLayout.models = listOf(
                NavigationItem(
                    "歌曲"
                ),
                NavigationItem(
                    "歌手"
                ),
                NavigationItem(
                    "歌单"
                )
            ).setInActiveColor(context.getColor(mandysax.tablayout.R.color.default_unchecked_color))
                .setActiveColor(context.getColor(R.color.theme_color))
            searchTabLayout.getSelectedPosition().observeForever {
                mBinding.searchViewPager.currentItem = it
            }
        }
    }

    private fun hideInput() {
        mBinding.search.searchEt.let {
            if (it.isFocused) {
                it.clearFocus()
                it.hideInput()
            }
        }
    }
}