package studio.mandysa.music.ui.activity.main.fragment.me.mylike

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mandysax.fragment.Fragment
import studio.mandysa.music.databinding.FragmentMyLikeBinding
import studio.mandysa.music.logic.ktx.viewBinding

class MyLikeFragment : Fragment() {
    private val mBinding: FragmentMyLikeBinding by viewBinding()

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }
}