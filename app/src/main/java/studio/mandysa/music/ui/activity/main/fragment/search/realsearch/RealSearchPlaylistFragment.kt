package studio.mandysa.music.ui.activity.main.fragment.search.realsearch

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import mandysax.fragment.Fragment
import mandysax.lifecycle.livedata.MutableLiveData
import studio.mandysa.music.R
import studio.mandysa.music.databinding.LayoutStateRvBinding
import studio.mandysa.music.logic.ktx.viewBinding

class RealSearchPlaylistFragment(mSearchContentLiveData: MutableLiveData<String>) : Fragment() {

    private val mBinding: LayoutStateRvBinding by viewBinding()

    private var mPage = 1

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        mBinding.apply {
            ViewCompat.setOnApplyWindowInsetsListener(root) { _, insets ->
                val navigationBarSize =
                    insets.getInsets(WindowInsetsCompat.Type.navigationBars()).bottom
                recycler.setPadding(
                    0,
                    0,
                    0,
                    context.resources.getDimensionPixelSize(R.dimen.controller_height) + context.resources.getDimensionPixelSize(
                        R.dimen.nav_height
                    ) + navigationBarSize
                )
                insets
            }
        }
    }

    fun nextPage() {
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }

}