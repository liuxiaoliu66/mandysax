package studio.mandysa.music.ui.activity.main.fragment.search.realsearch

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mandysax.anna2.exception.AnnaException
import mandysax.fragment.Fragment
import mandysax.lifecycle.coroutineScope
import mandysax.lifecycle.livedata.MutableLiveData
import studio.mandysa.jiuwo.utils.addFooter
import studio.mandysa.jiuwo.utils.linear
import studio.mandysa.jiuwo.utils.recyclerAdapter
import studio.mandysa.jiuwo.utils.setup
import studio.mandysa.music.R
import studio.mandysa.music.databinding.ItemSongBinding
import studio.mandysa.music.databinding.LayoutStateRvBinding
import studio.mandysa.music.logic.ktx.addBottom
import studio.mandysa.music.logic.ktx.markByColor
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.model.MusicModel
import studio.mandysa.music.logic.model.NeteaseCloudMusicApi
import studio.mandysa.music.logic.network.ServiceCreator
import studio.mandysa.music.service.playmanager.PlayManager
import studio.mandysa.music.service.playmanager.ktx.allArtist
import studio.mandysa.music.ui.activity.main.fragment.all.songmenu.SongMenuFragment

class RealSearchMusicFragment(private val mSearchContentLiveData: MutableLiveData<String>) :
    Fragment() {

    private val mBinding: LayoutStateRvBinding by viewBinding()

    private var mPage = 1

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        mBinding.apply {
            ViewCompat.setOnApplyWindowInsetsListener(root) { _, insets ->
                val navigationBarSize =
                    insets.getInsets(WindowInsetsCompat.Type.navigationBars()).bottom
                recycler.setPadding(
                    0,
                    0,
                    0,
                    context.resources.getDimensionPixelSize(R.dimen.controller_height) + context.resources.getDimensionPixelSize(
                        R.dimen.nav_height
                    ) + navigationBarSize
                )
                insets
            }
        }
        mBinding.stateLayout.showLoading {
            nextPage()
        }
        mBinding.recycler.addBottom {
            nextPage()
        }
        mBinding.recycler.linear().setup {
            addType<MusicModel>(R.layout.item_song)
            onBind {
                ItemSongBinding.bind(itemView).apply {
                    val model = getModel<MusicModel>()
                    songIndex.text = (modelPosition + 1).toString()
                    songName.text = model.title
                    songSingerName.text = model.artist.allArtist()
                    songName.markByColor(mSearchContentLiveData.value)
                    songSingerName.markByColor(mSearchContentLiveData.value)
                    itemView.setOnClickListener {
                        PlayManager.loadPlaylist(
                            models,
                            modelPosition
                        )
                    }
                    more.setOnClickListener {
                        SongMenuFragment(model).show(childFragmentManager)
                    }
                }
            }
        }
        mSearchContentLiveData.observe(viewLifecycleOwner) { it ->
            if (it.isNotEmpty()) {
                mBinding.let {
                    if (it.recycler.adapter != null && it.recycler.recyclerAdapter.mModel != null) {
                        it.recycler.scrollToPosition(0)
                        it.recycler.recyclerAdapter.clearModels()
                        mPage = 1
                    }
                    it.stateLayout.showLoadingState()
                }
            }
        }
    }

    private fun nextPage() {
        viewLifecycleOwner.lifecycle.coroutineScope.launch(Dispatchers.IO) {
            try {
                ServiceCreator.create(NeteaseCloudMusicApi::class.java).let { it ->
                    val model =
                        it.searchMusic(mSearchContentLiveData.value, (mPage - 1) * 30)
                            .execute()
                    val musicModel = it.getMusicInfo(model).execute()
                    withContext(Dispatchers.Main) {
                        musicModel.forEach {
                            mBinding.recycler.addFooter(it)
                        }
                        mBinding.stateLayout.showContentState()
                        mPage++
                    }
                }
            } catch (e: AnnaException) {
                mBinding.stateLayout.showErrorState()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }

}