package studio.mandysa.music.ui.activity.main.helper

import android.view.View
import com.sothree.slidinguppanel.positionhelper.impl.RecyclerViewScrollPositionHelper

class ViewPagerScrollPositionHelper : RecyclerViewScrollPositionHelper() {

    override fun isSupport(view: View): Boolean {
        if (view.toString().startsWith("androidx.viewpager2.widget.ViewPager2"))
            return false
        return super.isSupport(view)
    }

}