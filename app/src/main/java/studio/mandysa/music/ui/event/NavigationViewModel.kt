package studio.mandysa.music.ui.event

import mandysax.fragment.Fragment
import mandysax.lifecycle.ViewModel
import mandysax.lifecycle.livedata.LiveData
import mandysax.lifecycle.livedata.MutableLiveData

class NavigationViewModel : ViewModel() {

    private val mNavigationLiveData = MutableLiveData<Fragment?>()

    val fragmentLiveData: LiveData<Fragment?> = mNavigationLiveData

    fun navigation(fragment: Fragment) {
        mNavigationLiveData.value = fragment
        mNavigationLiveData.value = null
    }

}