package studio.mandysa.music.ui.activity.main.fragment.all.playlist

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import mandysax.fragment.Fragment
import mandysax.fragment.activityViewModels
import mandysax.fragment.viewModels
import studio.mandysa.jiuwo.utils.addFooter
import studio.mandysa.jiuwo.utils.addHeader
import studio.mandysa.jiuwo.utils.linear
import studio.mandysa.jiuwo.utils.setup
import studio.mandysa.music.R
import studio.mandysa.music.databinding.ItemPlaylistHeaderBinding
import studio.mandysa.music.databinding.ItemSongBinding
import studio.mandysa.music.databinding.LayoutStateRvBinding
import studio.mandysa.music.logic.ktx.addBottom
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.ktx.load
import studio.mandysa.music.logic.model.MusicModel
import studio.mandysa.music.logic.model.PlaylistInfoModel
import studio.mandysa.music.service.playmanager.PlayManager
import studio.mandysa.music.service.playmanager.ktx.allArtist
import studio.mandysa.music.ui.event.EventViewModel
import studio.mandysa.music.ui.activity.main.fragment.all.songmenu.SongMenuFragment

class PlaylistFragment(private val id: String) : Fragment() {

    private val mBinding: LayoutStateRvBinding by viewBinding()

    private val mViewModel: PlaylistViewModel by viewModels()

    private val mUserViewModel by activityViewModels<EventViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        mBinding.apply {
            ViewCompat.setOnApplyWindowInsetsListener(root) { _, insets ->
                val navigationBarSize =
                    insets.getInsets(WindowInsetsCompat.Type.navigationBars()).bottom
                recycler.setPadding(
                    0,
                    0,
                    0,
                    context.resources.getDimensionPixelSize(R.dimen.controller_height) + context.resources.getDimensionPixelSize(
                        R.dimen.nav_height
                    ) + navigationBarSize
                )
                insets
            }
        }
        mBinding.stateLayout.showLoading {
            mUserViewModel.getCookieLiveData().lazy(viewLifecycleOwner) { it ->
                mViewModel.bind(it, this@PlaylistFragment.id).observe(viewLifecycleOwner) { it1 ->
                    when (it1) {
                        is PlaylistViewModel.Status.Header -> {
                            mBinding.stateLayout.showContentState()
                            mBinding.recycler.addHeader(it1.value)
                        }
                        is PlaylistViewModel.Status.Next -> {
                            it1.value.forEach {
                                mBinding.recycler.addFooter(it)
                            }
                        }
                        is PlaylistViewModel.Status.Error -> {
                            mBinding.stateLayout.showErrorState()
                        }
                    }
                }
            }
        }
        mBinding.stateLayout.showLoadingState()
        mBinding.recycler.linear().setup {
            addType<PlaylistInfoModel>(R.layout.item_playlist_header) {
                val model = getModel<PlaylistInfoModel>()
                ItemPlaylistHeaderBinding.bind(itemView).apply {
                    playlistCover.load(model.coverImgUrl)
                    playlistTitle.text = model.name
                    playlistDescription.text = model.description
                }
            }
            addType<MusicModel>(R.layout.item_song) {
                val model = getModel<MusicModel>()
                ItemSongBinding.bind(itemView).apply {
                    songIndex.text = (modelPosition + 1).toString()
                    songName.text = model.title
                    songSingerName.text = model.artist.allArtist()
                    itemView.setOnClickListener {
                        PlayManager.loadPlaylist(
                            models,
                            modelPosition
                        )
                    }
                    more.setOnClickListener {
                        SongMenuFragment(model).show(childFragmentManager)
                    }
                }
            }
        }
        mBinding.recycler.addBottom {
            mViewModel.nextPage()
        }
    }

}