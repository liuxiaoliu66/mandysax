package studio.mandysa.music.ui.activity.main.fragment.login

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mandysax.fragment.DialogFragment
import studio.mandysa.music.databinding.FragmentLoginBinding
import studio.mandysa.music.logic.ktx.viewBinding

class LoginFragment : DialogFragment() {

    private val mBinding by viewBinding<FragmentLoginBinding>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return mBinding.root
    }

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        dialog!!.window!!.apply {
            setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
        mBinding.btnLogin.setOnClickListener {
            LoggingFragment(mBinding.etPhone.text.toString(), mBinding.etPassword.text.toString()).show(
                childFragmentManager
            )
        }
    }
}