package studio.mandysa.music.ui.activity.main.fragment.all.album

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mandysax.anna2.exception.AnnaException
import mandysax.lifecycle.ViewModel
import mandysax.lifecycle.livedata.LiveData
import mandysax.lifecycle.livedata.MutableLiveData
import mandysax.lifecycle.viewModelScope
import studio.mandysa.music.logic.model.AlbumContentModel
import studio.mandysa.music.logic.model.MusicModel
import studio.mandysa.music.logic.model.NeteaseCloudMusicApi
import studio.mandysa.music.logic.network.ServiceCreator

class AlbumViewModel : ViewModel() {
    sealed class Status {
        data class Header(val value: AlbumContentModel) : Status()
        data class Next(val value: List<MusicModel>) : Status()
        data class Error(val e: AnnaException) : Status()
    }

    private val mStatusLiveData = MutableLiveData<Status>()

    fun init(id: String): LiveData<Status> {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val model =
                    ServiceCreator.create(NeteaseCloudMusicApi::class.java).getAlbumContent(id)
                mStatusLiveData.postValue(Status.Header(model))
                mStatusLiveData.postValue(Status.Next(model.songList!!))
            } catch (e: AnnaException) {
                mStatusLiveData.postValue(Status.Error(e))
            }
        }
        return mStatusLiveData
    }

}