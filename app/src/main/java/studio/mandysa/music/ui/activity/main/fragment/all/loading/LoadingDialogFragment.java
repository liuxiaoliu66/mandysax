package studio.mandysa.music.ui.activity.main.fragment.all.loading;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import mandysax.fragment.DialogFragment;
import studio.mandysa.music.R;
import studio.mandysa.music.databinding.LayoutLoadingBinding;

public class LoadingDialogFragment extends DialogFragment {

    @NonNull
    @Override
    protected final View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        return LayoutLoadingBinding.inflate(inflater).getRoot();
    }

    @Override
    public final int getTheme() {
        return R.style.TransparentDialog;
    }
}
