package studio.mandysa.music.ui.activity.main

import android.os.Bundle
import com.yanzhenjie.sofia.Sofia
import mandysax.fragment.FragmentActivity
import studio.mandysa.music.databinding.ActivityMainBinding
import studio.mandysa.music.logic.ktx.viewBinding

class MainActivity : FragmentActivity() {

    private val mBinding: ActivityMainBinding by viewBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mBinding.root)
        Sofia.with(this@MainActivity).let {
            it.invasionStatusBar().invasionNavigationBar()
            it.statusBarDarkFont().navigationBarDarkFont()
        }
    }

}

