package studio.mandysa.music.ui.activity.main.fragment.all.songmenu

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mandysax.anna2.exception.AnnaException
import mandysax.fragment.DialogFragment
import mandysax.fragment.activityViewModels
import mandysax.lifecycle.coroutineScope
import studio.mandysa.jiuwo.utils.addFooter
import studio.mandysa.jiuwo.utils.addModels
import studio.mandysa.jiuwo.utils.linear
import studio.mandysa.jiuwo.utils.setup
import studio.mandysa.music.R
import studio.mandysa.music.databinding.FragmentSongMenuBinding
import studio.mandysa.music.databinding.ItemMenuBinding
import studio.mandysa.music.logic.ktx.load
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.model.NeteaseCloudMusicApi
import studio.mandysa.music.logic.network.ServiceCreator
import studio.mandysa.music.service.playmanager.ktx.allArtist
import studio.mandysa.music.service.playmanager.model.ArtistModel
import studio.mandysa.music.service.playmanager.model.MusicModel
import studio.mandysa.music.ui.activity.main.fragment.all.album.AlbumFragment
import studio.mandysa.music.ui.activity.main.fragment.all.singerpage.SingerPageFragment
import studio.mandysa.music.ui.event.EventViewModel
import studio.mandysa.music.ui.event.NavigationViewModel


class SongMenuFragment(private val mModel: MusicModel<*, *>) : DialogFragment() {

    sealed class Model {
        data class LikedSong(val id: String) : Model()
        data class AddPlaylist(val added: Boolean) : Model()
        data class Singer(val singer: ArtistModel) : Model()
        data class Album(val album: studio.mandysa.music.service.playmanager.model.AlbumModel)
        data class InStream(val any: Any?) : Model()
    }

    private val mBinding: FragmentSongMenuBinding by viewBinding()

    private val mNavigationViewModel: NavigationViewModel by activityViewModels()

    private val mEvent: EventViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }

    override fun getTheme(): Int {
        return R.style.TransparentDialog
    }

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        dialog!!.window!!.apply {
            setGravity(Gravity.BOTTOM)
            setWindowAnimations(R.style.AppDialogAnim)
            setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
        mBinding.apply {
            toolbar.more.visibility = View.GONE
            toolbar.songName.setTextColor(context.getColor(android.R.color.black))
            toolbar.songName.text = mModel.title
            toolbar.songSingerName.setTextColor(context.getColor(R.color.tv_color_light))
            toolbar.songSingerName.text = mModel.artist.allArtist()
            toolbar.songCover.load(mModel.coverUrl)
            recycler.linear().setup {
                addType<Model.LikedSong>(R.layout.item_menu) {
                    val model = getModel<Model.LikedSong>()
                    itemView.setIconAndText(
                        R.drawable.ic_round_favorite_border_24,
                        context.getString(R.string.add_to_my_likes)
                    )
                    itemView.isEnabled = false
                    mEvent.getCookieLiveData().lazy(viewLifecycleOwner) { cookie ->
                        mEvent.getUserIdLiveData().lazy(viewLifecycleOwner) { userid ->
                            viewLifecycleOwner.lifecycle.coroutineScope.launch(Dispatchers.IO) {
                                try {
                                    val list =
                                        ServiceCreator.create(NeteaseCloudMusicApi::class.java)
                                            .getLikeList(cookie!!, userid!!).execute()
                                    withContext(Dispatchers.Main) {
                                        (list.indexOf(model.id) != -1).let {
                                            itemView.setIconAndText(
                                                if (it) R.drawable.ic_round_favorite_24 else R.drawable.ic_round_favorite_border_24,
                                                context.getString(if (it) R.string.remove_from_my_likes else R.string.add_to_my_likes)
                                            )
                                        }
                                        itemView.isEnabled = true
                                    }
                                } catch (e: AnnaException) {
                                }
                            }
                        }
                    }

                }
                addType<Model.AddPlaylist>(R.layout.item_menu) {
                    itemView.setIconAndText(
                        R.drawable.ic_round_add_24,
                        "添加到歌单"
                    )
                }
                addType<Model.InStream>(R.layout.item_menu) {
                    itemView.setIconAndText(
                        R.drawable.ic_round_wrap_text_24,
                        "下一首播放"
                    )
                }
                addType<Model.Singer>(R.layout.item_menu) {
                    val model = getModel<Model.Singer>()
                    itemView.setIconAndText(
                        R.drawable.ic_round_person_24,
                        "歌手：${model.singer.name}"
                    )
                    itemView.setOnClickListener {
                        mNavigationViewModel.navigation(SingerPageFragment(model.singer.id))
                        dismiss()
                    }
                }
                addType<Model.Album>(R.layout.item_menu) {
                    val model = getModel<Model.Album>()
                    itemView.setIconAndText(
                        R.drawable.ic_round_album_24,
                        "专辑：${model.album.name}"
                    )
                    itemView.setOnClickListener {
                        mNavigationViewModel.navigation(AlbumFragment(model.album.id))
                        dismiss()
                    }
                }
            }
            recycler.addModels(
                listOf(
                    Model.LikedSong(mModel.id), Model.AddPlaylist(false),
                    Model.InStream(null), Model.Album(mModel.album)
                )
            )
            for (artistModel in mModel.artist) {
                recycler.addFooter(Model.Singer(artistModel))
            }
        }
    }

    private fun View.setIconAndText(@DrawableRes imageRes: Int, text: String) {
        ItemMenuBinding.bind(this).apply {
            iconAndText.setCompoundDrawablesWithIntrinsicBounds(
                resources.getDrawable(
                    imageRes,
                    null
                ), null, null, null
            )
            iconAndText.text = text
        }
    }
}