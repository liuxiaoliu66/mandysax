package studio.mandysa.music.ui.activity.main.fragment.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mandysax.fragment.Fragment
import mandysax.navigation.Navigation
import studio.mandysa.jiuwo.utils.linear
import studio.mandysa.jiuwo.utils.recyclerAdapter
import studio.mandysa.jiuwo.utils.setup
import studio.mandysa.music.R
import studio.mandysa.music.databinding.ItemSearchEditBinding
import studio.mandysa.music.databinding.ItemSubTitleBinding
import studio.mandysa.music.databinding.ItemTitleBinding
import studio.mandysa.music.databinding.LayoutRvBinding
import studio.mandysa.music.logic.ktx.viewBinding
import studio.mandysa.music.logic.model.design.SearchModel
import studio.mandysa.music.logic.model.design.SubTitleModel
import studio.mandysa.music.logic.model.design.TitleModel
import studio.mandysa.music.ui.activity.main.fragment.search.realsearch.RealSearchFragment

class SearchFragment : Fragment() {

    private val mBinding: LayoutRvBinding by viewBinding()

    override fun onViewCreated(view: View) {
        super.onViewCreated(view)
        mBinding.recycler.linear().setup {
            addType<TitleModel>(R.layout.item_title) {
                ItemTitleBinding.bind(itemView).titleTv.text =
                    getModel<TitleModel>().title
            }
            addType<SubTitleModel>(R.layout.item_sub_title) {
                ItemSubTitleBinding.bind(itemView).title.text =
                    getModel<SubTitleModel>().title
            }
            addType<SearchModel>(R.layout.item_search_edit) {
                ItemSearchEditBinding.bind(itemView).searchEt.setOnClickListener {
                    Navigation.findViewNavController(it).navigate(RealSearchFragment())
                }
            }
        }
        mBinding.recycler.recyclerAdapter.models =
            listOf(TitleModel(context.getString(R.string.search)), SearchModel())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup
    ): View {
        return mBinding.root
    }

}
