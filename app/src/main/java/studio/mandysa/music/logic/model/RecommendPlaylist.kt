package studio.mandysa.music.logic.model

import mandysax.anna2.annotation.Value

class RecommendPlaylist {

    @Value("recommend")
    val playlist: List<PlaylistModel>? = null

}