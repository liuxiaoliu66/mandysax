package studio.mandysa.music.logic.ktx

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

inline fun RecyclerView.addBottom(crossinline block: RecyclerView.() -> Unit) {

    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        // 最后几个完全可见项的位置（瀑布式布局会出现这种情况）
        private var lastCompletelyVisiblePositions: IntArray? = null

        // 最后一个完全可见项的位置
        private var lastCompletelyVisibleItemPosition = 0

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            // 找到最后一个完全可见项的位置
            when (val layoutManager = recyclerView.layoutManager) {
                is StaggeredGridLayoutManager -> {
                    if (lastCompletelyVisiblePositions == null) {
                        lastCompletelyVisiblePositions = IntArray(layoutManager.spanCount)
                    }
                    layoutManager.findLastCompletelyVisibleItemPositions(
                        lastCompletelyVisiblePositions
                    )
                    lastCompletelyVisibleItemPosition =
                        getMaxPosition(lastCompletelyVisiblePositions!!)
                }
                is GridLayoutManager -> {
                    lastCompletelyVisibleItemPosition =
                        layoutManager.findLastCompletelyVisibleItemPosition()
                }
                is LinearLayoutManager -> {
                    lastCompletelyVisibleItemPosition =
                        layoutManager.findLastCompletelyVisibleItemPosition()
                }
                else -> {
                    throw RuntimeException("Unsupported LayoutManager.")
                }
            }
        }

        private fun getMaxPosition(positions: IntArray): Int {
            var max = positions[0]
            for (i in 1 until positions.size) {
                if (positions[i] > max) {
                    max = positions[i]
                }
            }
            return max
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            val layoutManager = recyclerView.layoutManager
            // 通过比对 最后完全可见项位置 和 总条目数，来判断是否滑动到底部
            val visibleItemCount = layoutManager!!.childCount
            val totalItemCount = layoutManager.itemCount
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                if (visibleItemCount > 0 && lastCompletelyVisibleItemPosition >= totalItemCount - 1) {
                    block.invoke(this@addBottom)
                }
            }
        }
    })
}