package studio.mandysa.music.logic.ktx

import android.view.LayoutInflater
import androidx.viewbinding.ViewBinding
import mandysax.fragment.DialogFragment
import mandysax.fragment.Fragment
import mandysax.fragment.FragmentActivity
import mandysax.lifecycle.FullLifecycleObserver
import mandysax.lifecycle.LifecycleOwner
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

inline fun <reified VB : ViewBinding> FragmentActivity.viewBinding() = lazy {
    inflateBinding(VB::class.java, layoutInflater)
}

@Suppress("UNCHECKED_CAST")
fun <VB : ViewBinding> inflateBinding(clazz: Class<VB>, layoutInflater: LayoutInflater) =
    clazz.getMethod("inflate", LayoutInflater::class.java)
        .invoke(null, layoutInflater) as VB

inline fun <reified VB : ViewBinding> viewBinding() =
    FragmentBindingDelegate(VB::class.java)

class FragmentBindingDelegate<VB : ViewBinding>(
    private val clazz: Class<VB>
) : ReadOnlyProperty<Fragment, VB> {

    private var mBinding: VB? = null

    override fun getValue(thisRef: Fragment, property: KProperty<*>): VB {
        if (mBinding == null) {
            mBinding = inflateBinding(clazz, thisRef.layoutInflater)
            when (thisRef) {
                is DialogFragment -> {
                    thisRef.lifecycle.addObserver(object : FullLifecycleObserver {
                        override fun onDestroy(owner: LifecycleOwner) {
                            super.onDestroy(owner)
                            mBinding = null
                        }
                    })
                }
                else -> {
                    thisRef.viewLifecycleOwner.lifecycle.addObserver(object :
                        FullLifecycleObserver {
                        override fun onDestroy(owner: LifecycleOwner) {
                            super.onDestroy(owner)
                            mBinding = null
                        }
                    })
                }
            }
        }
        return mBinding!!
    }
}