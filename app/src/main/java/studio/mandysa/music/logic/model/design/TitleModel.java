package studio.mandysa.music.logic.model.design;

public final class TitleModel {
    private final String mTitle;

    public TitleModel(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }
}
