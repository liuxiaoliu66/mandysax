package studio.mandysa.music.logic.network;

/**
 * @author Huang hao
 */
public final class Url {
    public final static String MUSIC_URL = "https://music.163.com/song/media/outer/url?id=";
}
