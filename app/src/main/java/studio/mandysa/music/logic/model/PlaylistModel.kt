package studio.mandysa.music.logic.model

import mandysax.anna2.annotation.Value

class PlaylistModel {
    @Value("id")
    val id = ""

    @Value("name")
    val name = ""

    /*@Value("copywriter")
    public String info;*/
    @Value("picUrl")
    val picUrl = ""
}